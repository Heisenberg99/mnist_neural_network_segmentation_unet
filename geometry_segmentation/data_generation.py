import random
import cv2
import numpy as np

from colors_json import geometry_dict


# Geometry figures
def create_circle(center, radius, thickness, color, shape=(56, 56, 3)):
    image = np.zeros(shape=shape, dtype=int)
    image = cv2.circle(image,
                       center=center,
                       radius=radius,
                       color=(color['r'], color['g'], color['b']),
                       thickness=thickness, # толщина линии границы круга в пикселях. Толщина -1 px заполнит форму круга указанным цветом.
                       lineType=cv2.LINE_4, #  cv.FILLED, cv.LINE_4, cv.LINE_8, cv.LINE_AA
                       shift=0) # Количество дробных битов в координатах точки.
    return image


def create_square(pt1, pt2, thickness, color, shape=(56, 56, 3)):
    if pt2[0] - pt1[0] != pt2[1] - pt1[1]:
        raise "Error, it must be a square, not a rectangle"
    image = np.zeros(shape=shape, dtype=int)
    image = cv2.rectangle(image,
                          pt1=pt1,
                          pt2=pt2,
                          color=(color['r'], color['g'], color['b']),
                          thickness=thickness, # толщина линии границы круга в пикселях. Толщина -1 px заполнит форму указанным цветом.
                          lineType=cv2.LINE_4, # -1 4 8 16 
                          shift=0)
    return image


def create_rectangle(pt1, pt2, thickness, color, shape=(56, 56, 3)):
    if pt2[0] - pt1[0] == pt2[1] - pt1[1]:
        raise "Error, it must be a rectangle, not a square"
    image = np.zeros(shape=shape, dtype=int)
    image = cv2.rectangle(image,
                          pt1=pt1,
                          pt2=pt2,
                          color=(color['r'], color['g'], color['b']),
                          thickness=thickness, # толщина линии границы круга в пикселях. Толщина -1 px заполнит форму круга указанным цветом.
                          lineType=cv2.LINE_4, # -1 4 8 16 
                          shift=0)
    return image


def create_ellipse(center, axes, angle, startAngle, endAngle, thickness, color, shape=(56, 56, 3)):
    image = np.zeros(shape=shape, dtype=int)
    image = cv2.ellipse(image,
                        center=center,
                        axes=axes,
                        angle=angle,
                        startAngle=startAngle,
                        endAngle=endAngle,
                        color=(color['r'], color['g'], color['b']),
                        thickness=thickness)
    return image


def create_triangle(pt1, pt2, pt3, thickness, color, shape=(56, 56, 3)):
    image = np.zeros(shape=shape, dtype=int)
    points = np.array([pt1, pt2, pt3], dtype=int)
    image = cv2.polylines(image,
                          [points],
                          True,
                          color=(color['r'], color['g'], color['b']),
                          thickness=thickness)
    return image
    
    
# create random element
def generate_random_circle_obj():
    x_center_min = 19
    y_center_min = 19
    x_center_max = 56 - x_center_min
    y_center_max = 56 - y_center_min
    radius_min = 10
    radius_max = 16
    thickness_min = 1
    thickness_max = 3

    x_center = random.choice([i for i in range(x_center_min, x_center_max + 1)])
    y_center = random.choice([i for i in range(y_center_min, y_center_max + 1)])
    radius = random.choice([i for i in range(radius_min, radius_max + 1)])
    thickness = random.choice([i for i in range(thickness_min, thickness_max + 1)])

    params = {
        'center': (x_center, y_center),
        'radius': radius,
        'color': geometry_dict['circle'],
        'thickness': thickness
    }
    image = create_circle(**params)

    return image


def generate_random_square_obj():
    pt1_x_min = 5
    pt1_y_min = 5
    pt1_x_max = 23
    pt1_y_max = 23

    pt2_min = 10
    pt2_max = 30

    thickness_min = 1
    thickness_max = 2

    pt1_x = random.choice([i for i in range(pt1_x_min, pt1_x_max + 1)])
    pt1_y = random.choice([i for i in range(pt1_y_min, pt1_y_max + 1)])
    square_shift = random.choice([i for i in range(pt2_min, pt2_max + 1)]) 
    pt2_x = pt1_x + square_shift
    pt2_y = pt1_y + square_shift
    thickness = random.choice([i for i in range(thickness_min, thickness_max + 1)])

    params = {
        'pt1': (pt1_x, pt1_y),
        'pt2': (pt2_x, pt2_y),
        'color': geometry_dict['square'],
        'thickness': thickness
    }
    image = create_square(**params)

    return image


def generate_random_rectangle_obj():
    pt1_x_min = 5
    pt1_y_min = 5
    pt1_x_max = 23
    pt1_y_max = 23

    square_shift = 10

    thickness_min = 1
    thickness_max = 2

    pt1_x = random.choice([i for i in range(pt1_x_min, pt1_x_max + 1)])
    pt1_y = random.choice([i for i in range(pt1_y_min, pt1_y_max + 1)])

    shift_direction = random.choice(['x', 'y'])
    if shift_direction == 'x':
        x_rectangle_shift = random.choice([i for i in range(7, 20)])
        y_rectangle_shift = random.choice([i for i in range(4)])
    else:
        x_rectangle_shift = random.choice([i for i in range(4)])
        y_rectangle_shift = random.choice([i for i in range(7, 20)])

    pt2_x = pt1_x + square_shift + x_rectangle_shift
    pt2_y = pt1_y + square_shift + y_rectangle_shift
    thickness = random.choice([i for i in range(thickness_min, thickness_max + 1)])

    params = {
        'pt1': (pt1_x, pt1_y),
        'pt2': (pt2_x, pt2_y),
        'color': geometry_dict['rectangle'],
        'thickness': thickness
    }
    image = create_rectangle(**params)
    
    return image


def generate_random_ellipse_obj():
    x_center_min = 25
    y_center_min = 25
    x_center_max = 56 - x_center_min
    y_center_max = 56 - y_center_min

    x_axes_min = 5
    y_axes_min = 14

    x_axes_max = 9
    y_axes_max = 20

    angle_min = 0
    angle_max = 180

    thickness_min = 1
    thickness_max = 3

    x_center = random.choice([i for i in range(x_center_min, x_center_max + 1)])
    y_center = random.choice([i for i in range(y_center_min, y_center_max + 1)])
    x_axes = random.choice([i for i in range(x_axes_min, x_axes_max + 1)])
    y_axes = random.choice([i for i in range(y_axes_min, y_axes_max + 1)])
    angle = random.choice([i for i in range(angle_min, angle_max + 1)])
    thickness = random.choice([i for i in range(thickness_min, thickness_max + 1)])

    params = {
        'center': (x_center, y_center),
        'axes': (x_axes, y_axes), # большая и малая оси эллипса (длина большой оси, длина малой оси).
        'angle': angle, 
        'startAngle': 0,
        'endAngle': 360,
        'color': geometry_dict['ellipse'],
        'thickness': thickness
    }
    image = create_ellipse(**params)

    return image


def generate_random_triangle_obj():
    pt1_x_min = 2
    pt1_y_min = 2
    pt1_x_max = 18
    pt1_y_max = 18

    pt2_x_min = 53 - pt1_x_max
    pt2_y_min = pt1_y_max
    pt2_x_max = 53
    pt2_y_max = 2

    pt3_x_min = 2
    pt3_y_min = 52 - 18
    pt3_x_max = 52
    pt3_y_max = 52

    thickness_min = 1
    thickness_max = 3

    pt1_x = random.choice([i for i in range(pt1_x_min, pt1_x_max + 1)])
    pt1_y = random.choice([i for i in range(pt1_y_min, pt1_y_max + 1)])
    pt2_x = random.choice([i for i in range(pt2_x_min, pt2_x_max + 1)])
    pt2_y = random.choice([i for i in range(pt2_y_max, pt2_y_min + 1)])
    pt3_x = random.choice([i for i in range(pt3_x_min, pt3_x_max + 1)])
    pt3_y = random.choice([i for i in range(pt3_y_min, pt3_y_max + 1)])

    thickness = random.choice([i for i in range(thickness_min, thickness_max + 1)])

    params = {
        'pt1': [pt1_x, pt1_y],
        'pt2': [pt2_x, pt2_y],
        'pt3': [pt3_x, pt3_y],
        'color': geometry_dict['triangle'],
        'thickness': thickness,
    }
    image = create_triangle(**params)

    return image
