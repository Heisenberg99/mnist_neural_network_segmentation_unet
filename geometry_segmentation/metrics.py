import numpy as np
import copy
import pandas as pd
from keras import backend as k


def accuracy_metric(y_true, y_pred):
    y_true = copy.deepcopy(y_true).astype('int')
    y_pred = copy.deepcopy(y_pred).astype('int')
    y_true = np.where(y_true == 0, 0, 1)
    y_pred = np.where(y_pred == 0, 0, 1)
    return (y_true == y_pred).sum() / (y_pred.shape[0] * y_pred.shape[1] * y_pred.shape[2])


def multi_accuracy_metric(y_true, y_pred):
    y_true = copy.deepcopy(y_true).astype('int')
    y_pred = copy.deepcopy(y_pred).astype('int')
    return (y_true == y_pred).sum() / (y_pred.shape[0] * y_pred.shape[1] * y_pred.shape[2])


def iou_metric(y_true, y_pred):  # Jaccard Index
    y_true = copy.deepcopy(y_true).astype('float32') / 255.
    y_pred = copy.deepcopy(y_pred).astype('float32') / 255.
    return (k.sum(y_true * y_pred) + 1.) / (k.sum(y_true + y_pred) + 1.)


def dice_coef(y_true, y_pred):
    y_pred = copy.deepcopy(y_pred).astype('float32') / 255.
    y_true = copy.deepcopy(y_true).astype('float32') / 255.
    return (2. * k.sum(y_true * y_pred) + 1.) / (k.sum(y_true) + k.sum(y_pred) + 1.)


def calculate_metrics(model, x_data, y_data):
    metrics_df = pd.DataFrame(columns=['id', 'acc', 'm_acc', 'iou', 'dice'])
    predict_data = model.predict(x_data) * 255.
    predict_data = predict_data.astype('int')
    for i in range(x_data.shape[0]):
        metrics_df = metrics_df.append(
            {
                'id': i,
                'acc': accuracy_metric(y_data[i], predict_data[i]),
                'm_acc': multi_accuracy_metric(y_data[i], predict_data[i]),
                'iou': iou_metric(y_data[i], predict_data[i]).numpy(),
                'dice': dice_coef(y_data[i], predict_data[i]).numpy()
            }, ignore_index=True
        )
    return metrics_df
