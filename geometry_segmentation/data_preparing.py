import pickle
import numpy as np
import random
import matplotlib.pyplot as plt

from tqdm import tqdm_notebook



def save_data(data, data_name, folder_name='data'):
    model_bytes = pickle.dumps(data)
    file_path_template = "{0}/{1}"
    with open(file_path_template.format(f'./{folder_name}', data_name), 'wb') as model_file:
        model_file.write(model_bytes)


def load_data(data_name, folder_name='data'):
    file_path_template = "{0}/{1}"
    data = pickle.load(open(file_path_template.format(f'./{folder_name}', data_name), "rb"))
    return data


def train_test_split_custom(x_data, test_size):
    x_train_data = x_data[int(np.ceil(x_data.shape[0] * test_size)):]
    x_test_data = x_data[:int(x_data.shape[0] * test_size)]

    return x_train_data, x_test_data


def generate_img_64(data):
    temp_data_list = []

    for i in range(8):
        temp_data_list.append(
            np.array(
                np.concatenate((data[i*8], data[i*8+1], data[i*8+2], data[i*8+3],
                                data[i*8+4], data[i*8+5], data[i*8+6], data[i*8+7]), axis=1)
            )
        )
    temp_data = np.concatenate(temp_data_list)
    return temp_data[np.newaxis, :]


def create_data_64(data):
    first_flag = True
    for data_portion in tqdm_notebook(np.array_split(data, data.shape[0] // 64)):
        if first_flag:
            new_data = generate_img_64(data_portion)
            first_flag = False
        else:
            new_data = np.concatenate([new_data, generate_img_64(data_portion)], axis=0)
    return new_data


def add_white_elements(data):
    shape = ((data.shape[0] + (64 - data.shape[0] % 64)) * 2, data.shape[1], data.shape[2], data.shape[3])
    return np.append(data, np.zeros(shape, dtype='uint8'), axis=0)


def fill_coord_mask_image(coord_mask, shape=(56, 56, 3), color=(0, 255, 255)):
    image = np.zeros(shape=shape, dtype=int)
    for mask_elem in coord_mask:
        image[mask_elem[0]][mask_elem[1]] = color
    return image


def generate_x_data_from_mask(coord_mask_list):
    x_data_list = []
    for coord_mask in coord_mask_list:
        color = (
            random.choice([i for i in range(1, 255)]),
            random.choice([i for i in range(1, 255)]),
            random.choice([i for i in range(1, 255)])
        )
        x_data_list.append(fill_coord_mask_image(coord_mask, color=color))
    x_data = np.concatenate([x_data_list])

    return x_data


def plot_img(img_list, figsize=(5, 10), vmin=0, vmax=255):
    fig, ax = plt.subplots(nrows=1, ncols=len(img_list), figsize=figsize)
    if len(img_list) == 1:
        ax.imshow(img_list[0], vmin=vmin, vmax=vmax)
    else:
        for idx, img in enumerate(img_list):
            ax[idx].imshow(img, vmin=vmin, vmax=vmax)
    plt.show()


def generate_dataset(func, num_elements):
    temp_data_list = []
    for i in tqdm_notebook(range(num_elements)):
        temp_data_list.append(func()[np.newaxis, :])
    temp_data = np.concatenate(temp_data_list)

    return temp_data


def get_coordinates_mask(data):
    coordinates_list = []
    for num in tqdm_notebook(range(data.shape[0])):
        temp_elements_list = []
        for i in range(data.shape[1]):
            for j in range(data.shape[2]):
                for k in range(data.shape[3]):
                    if data[num][i][j][k] > 0:
                        temp_elements_list.append((i, j))
                        break
        coordinates_list.append(temp_elements_list)

    return coordinates_list
