import matplotlib.pyplot as plt
import numpy as np
import cv2
import copy
import pickle
import random
import os

from tqdm import tqdm_notebook
from albumentations import (
    CLAHE, RandomRotate90, RandomCrop, HueSaturationValue, GaussNoise,
    IAAPiecewiseAffine, IAASharpen, IAAEmboss, RandomContrast, RandomBrightness,
    HorizontalFlip, OneOf, Compose, Cutout
)
from colors_json import digit_dict


def get_mask_list(x_data):
    mask_list = []

    for num in tqdm_notebook(range(x_data.shape[0])):
        mask_list.append([])
        for i in range(x_data.shape[1]):
            for j in range(x_data.shape[2]):
                if x_data[num][i][j]:
                    mask_list[num].append((i, j))

    return mask_list


def get_black_images(x_data):
    for num in tqdm_notebook(range(x_data.shape[0])):
        x_data[num] = cv2.bitwise_not(x_data[num])
    return x_data


def add_new_chanels(old_data):
    data = np.zeros((old_data.shape[0],
                     old_data.shape[1],
                     old_data.shape[2], 3), dtype='uint8')
    for num in tqdm_notebook(range(old_data.shape[0])):
        for i in range(old_data.shape[1]):
            for j in range(old_data.shape[2]):
                if old_data[num][i][j][0]:
                    for k in range(3):
                        data[num][i][j][k] = old_data[num][i][j][0]
    return data


def return_y_data(mask_list, y_labels, shape=(70000,28,28,3)):
    y_data = np.zeros(shape=shape, dtype='uint8') # mask_list
    for num in tqdm_notebook(range(y_data.shape[0])):
        n_digit = y_labels[num]
        for i, j in mask_list[num]:
            y_data[num][i][j][0], y_data[num][i][j][1], y_data[num][i][j][2] = \
                digit_dict[n_digit]['r'], digit_dict[n_digit]['g'], digit_dict[n_digit]['b']
    return y_data


def save_data(data, data_name, folder_name='data'):
    model_bytes = pickle.dumps(data)
    file_path_template = "{0}/{1}"
    with open(file_path_template.format(f'./{folder_name}', data_name), 'wb') as model_file:
        model_file.write(model_bytes)


def load_data(data_name, folder_name='data'):
    file_path_template = "{0}/{1}"
    data = pickle.load(open(file_path_template.format(f'./{folder_name}', data_name), "rb"))
    return data


def plot_img(img_list, figsize=(5, 10), vmin=0, vmax=255):
    fig, ax = plt.subplots(nrows=1, ncols=len(img_list), figsize=figsize)
    if len(img_list) == 1:
        ax.imshow(img_list[0], vmin=vmin, vmax=vmax)
    else:
        for idx, img in enumerate(img_list):
            ax[idx].imshow(img, vmin=vmin, vmax=vmax)
    plt.show()


def add_background(x_data, img_type='white'):
    path_prefix = f'.\\backgrounds\\{img_type}'
    crop_aug = Compose([RandomCrop(224, 224)], p=1.0)
    backs_num_list = [i for i in range(len(os.listdir(path_prefix)))]
    backs_list = [cv2.imread(f'{path_prefix}\\background_{i}.jpg') for i in backs_num_list]

    for num in tqdm_notebook(range(x_data.shape[0])):
        back = backs_list[random.choice(backs_num_list)]
        x_data[num] = create_background(
            copy.deepcopy(x_data[num]),
            crop_aug(image=back)["image"],
            img_type
        )

    return x_data


def add_augmentation(x_data, y_data):
    augmentation = strong_aug(p=1.0)
    for i in tqdm_notebook(range(x_data.shape[0])):
        data = {'image': x_data[i], 'mask': y_data[i]}
        augmented = augmentation(**data)
        x_data[i], y_data[i] = augmented["image"], augmented["mask"]

    return x_data, y_data


def create_background(num_pict, back_part, back_stop_num=255):
    for i in range(num_pict.shape[0]):
        for j in range(num_pict.shape[1]):
            for k in range(num_pict.shape[2]):
                if num_pict[i][j][k] == back_stop_num:
                    num_pict[i][j][k] = int(back_part[i][j][k])
    return num_pict


def strong_aug(p=1.0):
    return Compose(
        [
            #RandomRotate90(p=.5),  # поворот
            #HorizontalFlip(p=0.2),  # зеркало
            Cutout(num_holes=random.choice([i for i in range(3, 10)]),
                   max_h_size=random.choice([i for i in range(1, 5)]),
                   max_w_size=random.choice([i for i in range(1, 5)]),
                   p=0.6),  # кропление
            GaussNoise(p=0.3),
            #IAAPiecewiseAffine(p=0.4),  # смазывается изображение
            OneOf([
                        CLAHE(clip_limit=random.choice([2, 3, 4])),  # контраст
                        IAASharpen(),  # резкость
                        IAAEmboss(),  # Тиснение
                        RandomContrast(),  # изменение контраста
                        RandomBrightness(),  # Яркость
                    ], p=0.4),
            HueSaturationValue(p=0.7)  # оттенок, насыщенность и значение входного изображения
        ],
        p=p
    )


def generate_img_64(data):
    temp_data_list = []

    for i in range(8):
        temp_data_list.append(
            np.array(
                np.concatenate((data[i*8], data[i*8+1], data[i*8+2], data[i*8+3],
                                data[i*8+4], data[i*8+5], data[i*8+6], data[i*8+7]), axis=1)
            )
        )
    temp_data = np.concatenate(temp_data_list)
    return temp_data[np.newaxis, :]


def create_data_64(data):
    first_flag = True
    for data_portion in tqdm_notebook(np.array_split(data, data.shape[0] // 64)):
        if first_flag:
            new_data = generate_img_64(data_portion)
            first_flag = False
        else:
            new_data = np.concatenate([new_data, generate_img_64(data_portion)], axis=0)
    return new_data


def add_white_elements(data):
    shape = ((data.shape[0] + (64 - data.shape[0] % 64)) * 2, data.shape[1], data.shape[2], data.shape[3])
    return np.append(data, np.zeros(shape, dtype='uint8'), axis=0)


def add_black_elements(data):
    shape = ((data.shape[0] + (64 - data.shape[0] % 64)) * 2, data.shape[1], data.shape[2], data.shape[3])
    return np.append(data, np.full(shape, 255, dtype='uint8'), axis=0)


def fill_coord_mask_image(coord_mask, shape=(56, 56, 3), color=(0, 255, 255)):
    image = np.full(shape=shape, fill_value=255, dtype=int)
    for mask_elem in coord_mask:
        image[mask_elem[0]][mask_elem[1]] = color
    return image


def generate_x_data_from_mask(coord_mask_list):
    x_data_list = []
    for coord_mask in coord_mask_list:
        color = (
            random.choice([i for i in range(1, 255)]),
            random.choice([i for i in range(1, 255)]),
            random.choice([i for i in range(1, 255)])
        )
        x_data_list.append(fill_coord_mask_image(coord_mask, color=color, shape=(28, 28, 3)))
    x_data = np.concatenate([x_data_list])

    return x_data


def change_shape_func(image, image_mask):
    shape = (224*2, 224*2, 3)
    guaranteed_shift = 30
    resize_size = random.choice(range(224, 224*2 - guaranteed_shift))
    direction = random.choice(range(0, 4))
    size_diff = 224*2 - resize_size - guaranteed_shift

    result_image = np.full(shape=shape, fill_value=255, dtype=int)
    result_mask = np.zeros(shape=shape, dtype=int)

    image = cv2.resize(image, (resize_size, resize_size))
    image_mask = cv2.resize(image_mask, (resize_size, resize_size))

    if direction == 0:
        for i in range(resize_size):
            for j in range(resize_size):
                result_image[i+guaranteed_shift][j+guaranteed_shift] = image[i][j]
                result_mask[i+guaranteed_shift][j+guaranteed_shift] = image_mask[i][j]
    elif direction == 1:
        for i in range(resize_size):
            for j in range(resize_size):
                result_image[i+guaranteed_shift][j+size_diff] = image[i][j]
                result_mask[i+guaranteed_shift][j+size_diff] = image_mask[i][j]
    elif direction == 2:
        for i in range(resize_size):
            for j in range(resize_size):
                result_image[i+size_diff][j+guaranteed_shift] = image[i][j]
                result_mask[i+size_diff][j+guaranteed_shift] = image_mask[i][j]
    elif direction == 3:
        for i in range(resize_size):
            for j in range(resize_size):
                result_image[i+size_diff][j+size_diff] = image[i][j]
                result_mask[i+size_diff][j+size_diff] = image_mask[i][j]

    result_image = result_image.astype('uint8')
    result_mask = result_mask.astype('uint8')
    
    return result_image, result_mask