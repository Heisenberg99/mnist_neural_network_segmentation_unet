import socket
import struct
import os
import numpy as np
import cv2
from keras.models import load_model
from configs import PORT_NUMBER, SEND_FORMAT
from utils import (
    dice_coef_loss, dice_coef_train
)


image_path_prefix = './server_images'
mask_path_prefix = './server_masks'
model_path_prefix = './model'
model_name = 'model.h5'


def receive_file_size(sckt):
    # Эта функция обеспечивает получение байтов, указывающих на размер отправляемого файла
    expected_bytes = struct.calcsize(SEND_FORMAT)
    received_bytes = 0
    stream = bytes()
    while received_bytes < expected_bytes:
        chunk = sckt.recv(expected_bytes - received_bytes)
        stream += chunk
        received_bytes += len(chunk)
    filesize = struct.unpack(SEND_FORMAT, stream)[0]
    return filesize


def receive_file(sckt, model, image_filepath, mask_filepath):
    # Сначала считываем из сокета количество байтов, которые будут получены из файла.
    filesize = receive_file_size(sckt)
    with open(image_filepath, "wb") as f:
        received_bytes = 0
        # Получаем данные из файла блоками по 1024 байта до объема бщего количество байт, сообщенных клиентом.
        while received_bytes < filesize:
            chunk = sckt.recv(1024)
            if chunk:
                f.write(chunk)
                received_bytes += len(chunk)
      
    image = cv2.imread(image_filepath)
    resize_shape = max(image.shape)
    if resize_shape % 224:
        resize_shape += 224 - (resize_shape % 224)
    image_predict = np.zeros(shape=(resize_shape, resize_shape, 3), dtype=int)
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            for k in range(3):
                image_predict[i][j][k] = image[i][j][k]
    mask_image = ((model.predict(np.array([image_predict])))[0] * 255.).astype('int')
    mask_result_predict = np.zeros(shape=(image.shape[0], image.shape[1], 3), dtype=int)
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            for k in range(3):
                mask_result_predict[i][j][k] = mask_image[i][j][k]
    cv2.imwrite(mask_filepath, mask_result_predict)


if __name__ == '__main__':
    model = load_model(
        f"{model_path_prefix}/{model_name}",
        custom_objects=
        {
            'dice_coef_loss': dice_coef_loss, 
            'dice_coef_train': [dice_coef_train]
        }
    )
    with socket.create_server(("localhost", PORT_NUMBER)) as server:
        while True:
            print("Ожидание клиента...")
            conn, address = server.accept()
            print(f"{address[0]}:{address[1]} подключен.")
            print("Получаем файл...")
            received_image_name = f"img_{len(os.listdir(image_path_prefix))}.png"
            received_mask_name = f"mask_{len(os.listdir(mask_path_prefix))}.png"
            receive_file(conn, model, f"{image_path_prefix}/{received_image_name}", f"{mask_path_prefix}/{received_mask_name}")
            print("Файл получен.")
    print("Соединение закрыто.")