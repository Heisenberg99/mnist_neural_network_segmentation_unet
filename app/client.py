import os
import socket
import struct
from configs import PORT_NUMBER, SEND_FORMAT


image_path_prefix = './client_images'


def send_file(sckt, filepath):
    filesize = os.path.getsize(filepath) # Получение размера файла.
    # <	- прямой порядок байтов (little-endian)	
    # Q - unsigned long long
    sckt.sendall(struct.pack(SEND_FORMAT, filesize)) # сколько байт будет отправлено. Возвращает объект bytes, содержащий значения v1, v2, …, упакованные в соответствии с форматом строки формата
    with open(filepath, "rb") as f: # Отправка файла чанками по 1024 байта.
        while read_bytes := f.read(1024):
            sckt.sendall(read_bytes)


if __name__ == '__main__':
    print("Введите название картинки:")
    filename = input() # "image_1.png"
    with socket.create_connection(("localhost", PORT_NUMBER)) as conn:
        print("Подключение к серверу.")
        print("Передача файла...")
        send_file(conn, f"{image_path_prefix}/{filename}")
        print("Отправлено.")
    print("Соединение закрыто.")