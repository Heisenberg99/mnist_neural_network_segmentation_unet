from keras import backend as K


def dice_coef_train(y_true, y_pred, smooth=1):
    y_true_f = K.flatten(y_true) # Протяни y_true в одно измерение.
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f * y_true_f) + K.sum(y_pred_f * y_pred_f) + smooth)


def dice_coef_loss(y_true, y_pred):
    return 1. - dice_coef_train(y_true, y_pred)
